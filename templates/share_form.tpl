{% include "header.tpl" %}

<div id="shareform">
   <form method="post" action="">
       <label for="title">Title: </label>
           <input type="text" name="title" id="title" />
         <br />
       <label for="url">Url: </label>
           <input type="text" name="url" id="url" />
        <br />
       <label for="tags">Tags (separated by spaces): </label>
           <input type="text" name="tags" id="tags" />
        <br />
       <label for="user">Username: </label>
           <input type="text" name="user" id="user" />
         <br />
       <label for="email">Email: </label>
           <input type="text" name="email" id="email"  />
        <br />
       <input type="submit" value="O.K, I'm ready to send it!" />
   </form>
</div>

{% include "footer.tpl" %}
