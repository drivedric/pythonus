{% include "header.tpl" %}

<div class="shared_url">
 <img src="{{ shared_url.email | gravatar(size=60) }}" />
   <h3><a href="{{ shared_url.url }}">{{ shared_url.title }}</a></h3>
      <p>
         Posted the {{ shared_url.create  }} by <a href="/user/{{ shared_url.user }}">{{ shared_url.user  }}</a>
          <br />
         <span class="tags">Tags: {{ shared_url.tags }}</span>
          <br />
      </p>
</div>

  <div id="disqus_thread" style="width: 60%;margin:auto;margin-top:100px;">
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_developer = 1;        
            var disqus_shortname = 'pythonus'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
</div>
{% include "footer.tpl" %}
