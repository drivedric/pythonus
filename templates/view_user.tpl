{% include "header.tpl" %}

{% if message is defined %}
    <h1 style="text-align:center">{{ message }}</h1>
{% else %}
{% for shared_url in shared_urls %}
    <div class="shared_url">
     <img src="{{ shared_url.email | gravatar(size=60) }}" />
       <h3><a href="{{ shared_url.url }}">{{ shared_url.title }}</a></h3>
          <p>
             Posted the {{ shared_url.create  }} by <a href="/user/{{ shared_url.user }}">{{ shared_url.user  }}</a>
              <br />
             <span class="tags">Tags: {{ shared_url.tags }}</span>
              <br />
             <span class="coms"><a href="/share/{{ shared_url.id  }}">View comments</a></span>
          </p>
    </div>
{% endfor %}
{% endif %}


{% include "footer.tpl" %}
