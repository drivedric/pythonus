{% include "header.tpl" %}

<h1>10 last shared urls</h1>

{% if page|int == 1 and nb_pages|int == 1 %}
    <p class="pages">Page {{ page }} of {{ nb_pages }}</p>
{% elif page|int == nb_pages|int  %}
    <p class="pages"><a href="?p={{ page - 1 }}"><< Prev </a> Page {{ page }} of {{ nb_pages }}</a></p>
{% elif page|int == 1 %}
    <p class="pages">Page {{ page }} of {{ nb_pages }} <a href="?p={{ page + 1 }}"> Next >></a></p>  
{% else %}
    <p class="pages"><a href="?p={{ page - 1 }}"><< Prev </a> Page {{ page }} of {{ nb_pages }} <a href="?p={{ page + 1 }}"> Next >></a></p>  
{% endif %}

{% for shared_url in shared_urls %}
<div class="shared_url">
 <img src="{{ shared_url.email | gravatar(size=65) }}" />
   <h3><a href="{{ shared_url.url }}">{{ shared_url.title }}</a></h3>
      <p>
         Posted the {{ shared_url.create  }} by <a href="/user/{{ shared_url.user }}">{{ shared_url.user  }}</a>
          <br />
         <span class="tags">Tags: {{ shared_url.tags }}</span>
          <br />
         <span class="coms"><a href="/share/{{ shared_url.id  }}">View comments</a></span>
      </p>
</div>
{% endfor %}

{% if page|int == 1 and nb_pages|int == 1 %}
    <p class="pages">Page {{ page }} of {{ nb_pages }}</p>
{% elif page|int == nb_pages|int  %}
    <p class="pages"><a href="?p={{ page - 1 }}"><< Prev </a> Page {{ page }} of {{ nb_pages }}</a></p>
{% elif page|int == 1 %}
    <p class="pages">Page {{ page }} of {{ nb_pages }} <a href="?p={{ page + 1 }}"> Next >></a></p>  
{% else %}
    <p class="pages"><a href="?p={{ page - 1 }}"><< Prev </a> Page {{ page }} of {{ nb_pages }} <a href="?p={{ page + 1 }}"> Next >></a></p>  
{% endif %}


{% include "footer.tpl" %}
