<!doctype html>
 <html lang="en">
   <head>
      <meta charset="utf-8">
      <!--<link rel="stylesheet" href="{{ url_for('static', filename='design.css') }}" />-->
      
      <meta name="keywords" content="python, url sharer, python url sharer"> 
      <meta name="description" content="Pythonus - A Python URL sharer using Flask, Jinja2 and SQLAlchemy">
      <meta name="robots" content="index, follow">

      <title>Pythonus - A Python URL sharer using Flask, Jinja2 and SQLAlchemy !</title>
   </head>
<body>
 
<header>

<a href="/">Python<span>US</span></a> 

<form style="float: right; margin-top: 15px; padding-right: 2%;" id="headerform" method="post" action="/search">
    <input type="text" name="search" placeholder="Search >" /> 
       <select name="searchopt">
           <option value="members">members</option>
           <option value="tags">tags</option>
       </select>
    <input type="submit" value="Search!" />
</form>

</header>

<div id="container">
   <form method="post" action="/share">
      <input type="text" name="title" id="title" placeholder="Title >" />
      <input type="text" name="url" id="url" placeholder="Url >" />
      <input type="text" name="tags" id="tags" placeholder="Tags >"/>
      <input type="text" name="user" id="user" placeholder="User >" />
      <input type="text" name="email" id="email" placeholder="Email >" />
      <input type="submit" value="Send it!" />
   </form>
</div>
<style>
@import url(http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz|Oswald|Lato|Droid+Serif);

::-moz-selection {
    color: #383431;
    background: #38a1ce;
}

::-webkit-selection {
    color: #383431;
    background: #38a1ce;
}

::selection { 
    color: #383431;
    background: #38a1ce;
}

html, body {
    margin: 0;
    padding: 0;
    width: 100%;
    height: 100%;
    background: url({{ url_for('static', filename='noisy_grid.png') }});
}

h1, h2, h3 {
    color: #38a1ce;
    font-family: "Yanone Kaffeesatz";
    margin-left: 1%;
}

header { 
    height: 54px;
    width: 100%;
    background: #383431;
}

header a {
    font-size: 200%;
    font-family: Oswald;
    color: #ffffff;
    padding-left: 1%;
    text-decoration: none;
}

header a span {
    color: #38a1ce;
    font-size: 100%;
}

.pages {
    color: #383431;
    font-family: Lato;
    font-size: 90%;
    padding-left: 1%;	
}

.pages a {
    color: #38a1ce;
    font-family: "Droid Serif";
    font-size: 80%;
}

.pages a:hover {
    color: #383431;	
}

.shared_url {
    height: 80px;
    width: 97%;
    margin-left: 1%;
    margin-right: 1%;
    background: #383431; 
}

.shared_url:hover {
    background: #38a1ce; 
}

.shared_url img {
    float: left;
    padding-left: 7px;
    padding-top: 7px;
}

.shared_url h3 a {
    font-size:80%;
    color: #ffffff;
    padding-top: 10px;
    padding-left: 7px;
    font-family: "Droid Serif";
}

.shared_url  a:hover {
    color: #383431; 
}

.shared_url p a:hover {
    color: #383431;
}

.shared_url p {
    font-size: 90%;
    text-align: justify;
    width: 400px;
    padding-left: 79px;
    height: 20px;
    min-height: 20px;
    margin-top: -20px;
    color: #ffffff;
    font-family: Lato;
}

.shared_url a {
    color: #bbbbbb;
    text-decoration: none;
}

.shared_url .tags a, .shared_url .coms a {
    color: #ffffff;
}


#container form { 
   padding-left: 1%;
   height: 22px;
   background: #38a1ce;
}

#container form input {
    background: #38a1ce;
    border: #38a1ce;
    color: #ffffff;
    font-family: Lato; 
}

input::-webkit-input-placeholder {
    color: #ffffff;
}

input::-moz-input-placeholder {
    color: #ffffff;
}

header form select {
    background: #38a1ce;
    border: 1px solid #383431;
    color: #ffffff;
}

header input {
    background: #383431;
    border: #383431;
    border-bottom: #ffffff;
    color: #ffffff;
}

footer {
    padding-left: 1%;
    padding-bottom: 5px;
}
footer p {
    color: #777777;
    font-size: 90%;
    font-family: Lato;
    font-style: italic;
}

footer p a {
    color: #666666;
}

#shareform {
    margin: auto;
    margin-top: 50px;
    text-align: center;
    width: 197px;    
}

#shareform form input {
    background: #383431; 
    border: #383431;
    width: 200px;
    color: #ffffff;
    font-family: Lato;
}

#shareform form label {
    color: #38a1ce;
    font-size: 105%;
    font-family: Lato;
}
</style>
