# -*- coding: utf-8 -*- 

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from datetime import datetime

PATH_TO_DB = 'sqlite:////home/drivedric/python/pythonus/db/db.sql'

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = PATH_TO_DB

db = SQLAlchemy(app)

class Share(db.Model):
    id     = db.Column(db.Integer, primary_key=True, unique=True)
    title  = db.Column(db.String)
    user   = db.Column(db.String)
    email  = db.Column(db.String)
    create = db.Column(db.String)
    tags   = db.Column(db.String)
    url    = db.Column(db.String)

    def __init__(self, title, user, email, tags, url):
        self.title  = title
        self.user   = user
        self.email  = email
        self.create = ' at '.join(str(datetime.utcnow()).split('.')[0].split(' '))
        self.tags   = tags
        self.url    = url


class Members(db.Model):
    id       = db.Column(db.Integer, primary_key=True, unique=True)
    username = db.Column(db.String)
    email    = db.Column(db.String)
    password = db.Column(db.String)

    def __init__(self, username, email, password):
        self.username = username
        self.email    = email
        self.password = password
