#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""

Pythonus - A Python url sharer using Flask, Jinja2 and SQLAlchemy

version: 1.0.0 
author: drivedric <drivedric@gmail.com> 
website: http://pythonus.drivedric.com

You can fork this project at https://bitbucket.org/drivedric/pythonus/fork

"""

import urllib, re
from flask import Flask, request, make_response, render_template, abort, redirect, url_for
from db.databaseManager import db, Members, Share
from flaskext import gravatar 

app = Flask(__name__) 

gravatar = gravatar.Gravatar(app,
                    size=150,
                    rating="g",
                    default="identicon",
                    force_default=False,
                    force_lower=False)



@app.route('/')
def index():
    """ 
    Index show: 
        - A list of the 10 latest urls 
        - A list of the latest pages 
        - A form to share a url
                        
    """

    page = int(request.args.get('p')) if request.args.get('p') is not None else 1 # if GET page exist, GET page, else 1
    nb_pages = (len(Share.query.all()) / 10) + 1 # the number of pages ; total/url by page 

      
    limit_page = 10 * page # the max ID by page (ex: 10*2 = 20, ID of page 2 begin at 20) 
    cut_page   = 10 * (page - 1) # page x show urls from ID 10*page -1 to 10*page. Ex: page 2 show url from ID 10*(2-1) = 10 to ID 10*2 = 20. 

    shared_urls = Share.query.order_by(Share.id.desc()).limit(limit_page).all() # select the urls from `limit_page` ID. Ex: 20
    shared_urls = shared_urls[cut_page:] # take from `cut_page` to the end. Ex: 10 to 20 (for page 2)

    if shared_urls == []: # no shared urls
        return render_template('error.tpl', message='No shared urls') 
 
    return render_template('index.tpl', shared_urls=shared_urls, page=page, nb_pages=nb_pages)


        
@app.route('/share', methods = ['GET', 'POST'])
def share():
    """  
    This page has two functions:
        - If the method is POST, check data and send it in the database
        - If the method is GET, show the form for share a url
    """

    if request.method == 'GET': # the form is not send 
        return render_template('share_form.tpl')
    
    
    data = request.form # shortcut!

    # check if some of the fields has been deleted
    if data['title'] is None or data['url'] is None or data['tags'] is None or data['user'] is None or data['email'] is None: 
        return render_template('form_prob.tpl')

    for key, value in data.items(): # check if data from the form is empty or not...
        if value  == '': # It's empty! Return an error  
            return render_template('error_form.tpl')
        else: 
            continue # no error, nothing to do

    #  check if url is valid
    try:
        urllib.urlopen(data['url'])
    except IOError:
        return render_template('error.tpl', message='Error: URL invalid. Please try again') 

    # check if mail is valid
    if re.match("^[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$", data['email']) is None:
        return render_template('error.tpl', message='Error: email is invalid. Please try again')
    

    # the `tags` is a string like "tag1 tag2 tag3", 
    # so we need to format the string to put the tags between `<a></a>`.
    # the line that follow do that by converting the string to a list, then
    #  adding the `<a></a>` between each tag, then reconverting the list to a string
    tags = ' '.join('<a href="/tags/{}">{}</a>'.format(x,x) for x in data['tags'].split(' '))

    # Putting data in the database
    share = Share(data['title'], data['user'], data['email'], tags, data['url'])
    db.session.add(share)
    db.session.commit() 
    
    return render_template('form_successful.tpl')
    


@app.route('/share/<id>')
def view_shared(id):
    """ View the shared url and the comments """ 

    try: 
        id = int(id) # check if <id> is a valid number...
    except ValueError:
        abort(404) # and return a 404 error if not 

    shared_url = Share.query.filter_by(id=id).first_or_404() # select the data and return 404 error if <id> is not in db 

    return render_template('view_url.tpl', shared_url=shared_url)



@app.route('/search', methods=['GET', 'POST'])
def search():
    """ Search for members or tags """

    if request.method == 'GET': # if method is not POST, nothing to do
        abort(404)
    
    data = request.form # shortcut!

    # check if some of the fields has been deleted
    if data['search'] is None or data['searchopt'] is None:
        return render_template('form_prob.tpl')

    # check if fields is empty
    if not data['search'] == '' and not data['searchopt'] == '':
        if data['searchopt'] == 'members': 
            return redirect(url_for('user', user=data['search'])) # if the user search for a members, redirect to /members/the_members
        elif data['searchopt'] == 'tags':
            return redirect(url_for('tags', tag=data['search'])) # if user search for a tags, redirect to /tags/the_tag
        else:
            return render_template('form_prob.tpl') # problem with form 

    return render_template('form_prob.tpl')        



@app.route('/tags/<tag>')
def tags(tag):
    """ View the shared urls where one of the tags is <tag> """   

    shared_urls = Share.query.filter(Share.tags.like('%' + tag + '%')).all() 
 
    if shared_urls == []: # no shared urls find
        return render_template('error.tpl', message='Error: no shared urls find')

    return render_template('view_user.tpl', shared_urls=shared_urls)  



@app.route('/user/<user>')
def user(user):
    """ View the user's shared urls """

    shared_urls = Share.query.filter_by(user=user).order_by(Share.id.desc()).all() # list of shared from <user> 
   
    if shared_urls == []: # no shared_urls for this user
        return render_template('error.tpl', message='No shared urls from this user')

    return render_template('view_user.tpl', shared_urls=shared_urls)



if __name__ == "__main__":
    app.run(debug=True)
